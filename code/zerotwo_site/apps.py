from django.apps import AppConfig


class ZerotwoSiteConfig(AppConfig):
    name = 'zerotwo_site'
