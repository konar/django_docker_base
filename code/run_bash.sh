#! /bin/bash
# Launches bash inside the container
# Code by Jędrzej Boczar

# docker container of the project
dockerProcessNr=$(docker ps | grep zerotwo_web | cut -d ' ' -f 1)

#aliasy
aliases="alias manage='./manage.py'; alias shell='clear && manage shell_plus'; alias test='clear && clear && manage test'"

# init file
TMPFILE='.tmpBashInit'
echo $aliases > $TMPFILE
trap "rm -f $TMPFILE" EXIT

# startuje basha korzystając z naszego pliku inicjalizacyjnego
# ustawia TERM na naszą wartość, dzięki czemu powinno być kolorowe
docker exec -it $dockerProcessNr env TERM=$TERM bash --init-file $TMPFILE
