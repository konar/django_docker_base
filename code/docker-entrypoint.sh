#!/bin/bash
# Code by Adam Prochownik & Piotr Matuszak
ownership() {
    # Fixes ownership of output files
    # source: https://github.com/BD2KGenomics/cgl-docker-lib/blob/master/mutect/runtime/wrapper.sh#L5
    user_id=$(stat -c '%u:%g' /code)
    chown -R ${user_id} /code
}
./wait-for-it.sh zerotwo_postgres:5432
echo ''
echo '--------------------------'
echo 'Database migration'
echo '--------------------------'
echo ''
python manage.py makemigrations
python manage.py migrate
python manage.py makemigrations zerotwo_site
python manage.py migrate zerotwo_site
python manage.py makemigrations
python manage.py migrate
echo ''
echo '--------------------------'
echo 'Localization migration'
echo '--------------------------'
echo ''
django-admin makemessages -l pl
django-admin compilemessages
echo ''
echo '--------------------------'
echo 'Run test'
echo '--------------------------'
echo ''
./manage.py test --noinput
echo ''
echo '--------------------------'
echo 'Generate static files folder'
echo '--------------------------'
echo ''
./manage.py collectstatic --noinput
echo ''
echo '--------------------------'
echo 'Fixing ownership of files'
echo '--------------------------'
echo ''
ownership
echo ''
echo '--------------------------'
echo 'Run server'
echo '--------------------------'
echo ''
python manage.py runserver 0.0.0.0:80

